Introduction:

	Youtube palylist finder basically fetch playlist items from youtube and create event on wynk. 

Overview :

	First of all we fetch the playlistItmes by using google api and then cast the responded data into 		our wynk model and then pass that particular model to the wynk create event api. Youtube finder 	project is created using dropwizard framework.

Running The Application:

	To test the example application run the following commands.

	1. To clean package the example run:

		mvn clean package

	2. To run jar file on server, run command:

		java -jar target/youtube-playlist-finder-0.0.1-SNAPSHOT.jar

To Test the application use following url:

	http://localhost:8074/api/v1/playlists/PL6DlDy7xEqYC75ypqBGpjGgVZkPyKzvb

Project Configurations:

	Project Configuration is mentioned in application.yaml file. 
	Admin is running on 8076 port.
	We can test health check of this project by using below url.
	
	http://localhost:8076/
	
	We can test following things on admin port.
	
	Metrics
	Ping
	Threads
	Healthcheck
	
Swagger Integration: 

	In this project swagger api documentations is integrated to test apis and maintain updated apis 	data. Also sonar testing tools is integrated to optimize the project code.   	

Sonar is running On: 

	http://localhost:9000/dashboard/index/com.wynk:youtube-playlist-finder

Sonar testing Result: 
	
	Bug : 0
	Duplication : 0.0%
	Vulnerabilities : 0
	Duplicated Blocks : 0
 
 Collections: You can find the collections of swagger and postman on below links.
  
	https://www.getpostman.com/collections/08664484a4494662f52a
	http://128.199.127.18:8074/swagger
	