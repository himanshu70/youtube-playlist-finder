package com.wynk.youtube.playlist.finder.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("PlayList")
@Path("/api/v1/playlists")
public interface PlayListResource {
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{playlist_id}")
	@ApiOperation("Get Play List Item")
	Response getPlayListItems(@PathParam("playlist_id") String playListId);
}
