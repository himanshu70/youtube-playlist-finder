package com.wynk.youtube.playlist.finder.provider;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Service;

import com.wynk.youtube.playlist.finder.exception.CustomException;

@Provider
@Service
public class CustomExceptionMapper implements ExceptionMapper<CustomException> {

	@Override
	public Response toResponse(final CustomException exception) {
		 return Response.status(exception.getStatus())
				 .type(MediaType.APPLICATION_JSON)
                 .entity(new Message(exception.getMessage())).build();
	}
}
