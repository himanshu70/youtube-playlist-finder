package com.wynk.youtube.playlist.finder.http.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.ServerErrorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpStatusCodeException;

import com.wynk.youtube.playlist.finder.exception.CustomException;
import com.wynk.youtube.playlist.finder.exception.PlayListNotFoundException;
import com.wynk.youtube.playlist.finder.exception.UnAuthorizedException;
import com.wynk.youtube.playlist.finder.http.BaseRequester;
import com.wynk.youtube.playlist.finder.http.YoutubeApiRequester;
import com.wynk.youtube.playlist.finder.model.PlayList;
import com.wynk.youtube.playlist.finder.util.Constants;
import com.wynk.youtube.playlist.finder.util.HeaderParam;

@Repository
public class YoutubeApiRequesterImpl implements YoutubeApiRequester {

	private final String apiKey;

	private final BaseRequester baseHttpRequester;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(YoutubeApiRequesterImpl.class);

	public YoutubeApiRequesterImpl(@Value("${api.key}") final String apikey,
			@Value("${youtube.root.url}") final String rootUrl,
			final BaseRequesterFactory httpRequesterFactory) {
		this.apiKey = apikey;
		this.baseHttpRequester = httpRequesterFactory.createBaseHttpReqester(
				"youtube_api.xml", rootUrl);
	}

	/*
	 * Fetch PlayList Items From Youtube By PlayListId
	 */

	@Override
	public Optional<PlayList> findPlayListItems(final String id) {
		final HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl(HeaderParam.CACHE_CONTROL);
		final Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		params.put("key", apiKey);
		try {
			final ResponseEntity<PlayList> response = baseHttpRequester
					.getRestTemplate().exchange(
							URLParamMapper.map(baseHttpRequester
									.getURLById(Constants.PLAYLIST_ITEMS_URL),
									params), HttpMethod.GET, null,
							PlayList.class);

			if (response.getStatusCode() == HttpStatus.OK) {
				return Optional.ofNullable(response.getBody());
			} else if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
				throw new PlayListNotFoundException(
						"Video Not Found For PlayList");
			} else if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				throw new UnAuthorizedException("Api Key is not Valid ");
			} else if (response.getStatusCode() == HttpStatus.NO_CONTENT) {
				return Optional.empty();
			} 
			LOGGER.error("Error while fetching playList items for id ={} ", id);
			throw new ServerErrorException(response.getStatusCode().value());
		} catch (HttpStatusCodeException e) {
			if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
				throw new CustomException(HttpStatus.BAD_REQUEST.value(),
						"Invalid Data ");
			} else if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
				throw new CustomException(HttpStatus.NOT_FOUND.value(),
						"Error in Youtube  Url");
			}
		} catch (Exception e) {
			throw new ServerErrorException(
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return Optional.empty();
	}

}
