package com.wynk.youtube.playlist.finder.http;

import java.util.Optional;

import com.wynk.youtube.playlist.finder.model.PlayList;

public interface YoutubeApiRequester {
   
	 Optional<PlayList> findPlayListItems(String id);
}
