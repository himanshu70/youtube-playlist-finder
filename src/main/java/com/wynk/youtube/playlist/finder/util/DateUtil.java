package com.wynk.youtube.playlist.finder.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateUtil.class);

	public static Long getDateString(final String date) {
		SimpleDateFormat format = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date parsedTimeStamp = null;
		try {
			parsedTimeStamp = format.parse(date);
			return parsedTimeStamp.getTime();
		} catch (ParseException e) {
			LOGGER.error("Error while parsing date = {}", date.toString(), e);
		}
		return 0L;
	}
}
