package com.wynk.youtube.playlist.finder.exception;

public class PlayListNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PlayListNotFoundException(final String message) {
		super(message);
	}
	
	public PlayListNotFoundException(final String message,final Throwable cause) {
		super(message,cause);
	}

}
